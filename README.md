# Package R version 3.6.0


Package installation using Miniconda3 V4.7.12 All packages are in /opt/miniconda/bin & are in PATH R Version: 3.6.0
Singularity container based on the recipe: Singularity.R_3.6.0
## Local build:
```bash
sudo singularity build R_base_v3.6.0.sif Singularity.R_3.6.0
```
## Get image help:

```bash
singularity run-help R_base_v3.6.0.sif
```
### Default runscript: gatk4
## Usage:
```bash
./R_base_v3.6.0.sif --help
```
or:
```bash
singularity exec R_base_v3.6.0.sif R --help
```
image singularity (V>=3.3) is automacly built (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml
can be pull (singularity version >=3.3) with:

```bash
singularity pull R_base_v3.6.0.sif oras://registry.forgemia.inra.fr/singularity/bioinfo/R_base_v3.6.0/R_base_v3.6.0:latest